<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>2016531047</title>
<script src="https://use.fontawesome.com/e7683fce0b.js"></script>
<script src="Master.js"></script>
<link rel="stylesheet" type="text/css" href="Master.css">
<link rel="stylesheet" type="text/css" href="Project.css">	
<link rel="stylesheet" type="text/css" href="ProductManagement.css">	
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>
<body>
 <div id="divHeader">
	<span class="spanLogoMain" onclick="goHome()"></span>
	<span id="spanHome" onclick="goHome()">Summer Term Project</span>
	<p class="pBtnMainEnd" onclick="goPage('Project')">Project</p>
	<p class="pBtnMainLine2" onclick="goPage('ProductManagement')">Product Management</p>
	<p class="pBtnMain" onclick="goPage('Task')">Task</p>
	<p class="pBtnMain" onclick="goPage('Groupware')">Groupware</p>
</div>
<div id="divTitle">Product Management</div>
<div id="divAccordion">
  <h3 class="h3Title" onclick="MoveAnimation('B');">BackUp<i class="fa fa-angle-down arrowProcess"></i></h3>
  <div class="divContent">
    <p>모든 서버의 소스와 데이터베이스를 백업합니다. 데이터베이스는 유지관리계획으로도 백업하기도 합니다. 소스는 형상관리도구도 백업합니다.</p>
  </div>
  <h3 class="h3Title" onclick="MoveAnimation('D');">Distribute<i class="fa fa-angle-down arrowProcess"></i></h3>
  <div class="divContent">
    <p>회사 내부에서 쓰는 배치 파일을 가지고 각 서버에 소스를 배포합니다. 수정날짜를 기준으로 하며 각 팀 담당 소스는 각 팀에서 배포합니다.
    데이터베이스는 비주얼 스튜디오에 지원되는 기능으로 비교하며 배포하거나 작은 것은 수동으로 배포합니다.</p>
  </div>
  <h3 class="h3Title" onclick="MoveAnimation('EI');">Error & Improvement<i class="fa fa-angle-down arrowProcess"></i></h3>
  <div class="divContent">
    <p>회사 운영 게시판에 오류사항과 개선사항 게시물이 등록되면 해당 내용이 실제로 오류사항이 맞는지 테스트하고, 실제 발생할 경우 수정하게 됩니다.
    개선사항은 제품파트장의 검토 후 개발을 진행하게 됩니다. 이렇게 고쳐진 오류사항들이 다시 배포되게 됩니다.</p>
  </div>
</div>
<div id="divFooter">
	<span class="spanLogo"></span>
	<span class="spanFooter"><i class="fa fa-copyright" aria-hidden="true"></i> KPU 2016531047 JMJ</span>
	<span class="spanFooter"><i class="fa fa-font" aria-hidden="true"></i> Nanum Gothic (GoogleAPI : earlyaccess)</span>
	<span class="spanFooter"><i class="fa fa-file-image-o" aria-hidden="true"></i> Font Awesome</span>
	<span class="spanFooter"><i class="fa fa-phone" aria-hidden="true"></i> 010-2113-9911</span>
	<span class="spanFooter"><i class="fa fa-envelope" aria-hidden="true"></i> alswjd135792@gmail.com</span>
</div> 
<script>
	$("#divAccordion" ).accordion();
	
	function MoveAnimation(pType) {
		if(pType == "B") {
			
		} else if(pType == "D") {
			
		} else if(pType == "EI") {
			
		} 
	}
</script>
</body>
</html>
