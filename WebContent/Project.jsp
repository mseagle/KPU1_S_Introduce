<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>2016531047</title>
<script src="https://use.fontawesome.com/e7683fce0b.js"></script>
<script src="Master.js"></script>
<link rel="stylesheet" type="text/css" href="Master.css">
<link rel="stylesheet" type="text/css" href="Project.css">	
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>
<body>
 <div id="divHeader">
	<span class="spanLogoMain" onclick="goHome()"></span>
	<span id="spanHome" onclick="goHome()">Summer Term Project</span>
	<p class="pBtnMainEnd" onclick="goPage('Project')">Project</p>
	<p class="pBtnMainLine2" onclick="goPage('ProductManagement')">Product Management</p>
	<p class="pBtnMain" onclick="goPage('Task')">Task</p>
	<p class="pBtnMain" onclick="goPage('Groupware')">Groupware</p>
</div>
<div id="divTitle">Project</div>
<div id="divAccordion">
  <h3 class="h3Title">분석 및 설계<i class="fa fa-angle-down arrowProcess" aria-hidden="true"></i></h3>
  <div class="divContent">
    <li>인프라 환경 구축</li>
    <li>AS-IS 분석</li>
    <li>요구사항 분석</li>
    <li>시스템 설계</li>
    <li>화면 및 기능설계</li>
    <li>마이그레이션 설계</li>
  </div>
  <h3 class="h3Title">구현</h3>
  <div class="divContent">
    <p>개발 진행</p>
  </div>
  <h3 class="h3Title">배포 및 이행</h3>
  <div class="divContent">
    <ul>
      <li>운영 반영</li>
      <li>테스트(단위, 통합 등)</li>
      <li>메뉴얼 등 문서 전달</li>
    </ul>
  </div>
</div>
<div id="divFooter">
	<span class="spanLogo"></span>
	<span class="spanFooter"><i class="fa fa-copyright" aria-hidden="true"></i> KPU 2016531047 JMJ</span>
	<span class="spanFooter"><i class="fa fa-font" aria-hidden="true"></i> Nanum Gothic (GoogleAPI : earlyaccess)</span>
	<span class="spanFooter"><i class="fa fa-file-image-o" aria-hidden="true"></i> Font Awesome</span>
	<span class="spanFooter"><i class="fa fa-phone" aria-hidden="true"></i> 010-2113-9911</span>
	<span class="spanFooter"><i class="fa fa-envelope" aria-hidden="true"></i> alswjd135792@gmail.com</span>
</div> 
<script>
	$("#divAccordion" ).accordion();
</script>
</body>
</html>
