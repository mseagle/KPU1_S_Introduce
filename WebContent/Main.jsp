<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>2016531047</title>
<script src="https://use.fontawesome.com/e7683fce0b.js"></script>
<script src="Master.js"></script>
<link rel="stylesheet" type="text/css" href="Master.css">
</head>
<body>
<div id="divHeader">
	<span class="spanLogoMain" onclick="goHome()"></span>
	<span id="spanHome" onclick="goHome()">Summer Term Project</span>
	<p class="pBtnMainEnd" onclick="goPage('Project')">Project</p>
	<p class="pBtnMainLine2" onclick="goPage('ProductManagement')">Product Management</p>
	<p class="pBtnMain" onclick="goPage('Task')">Task</p>
	<p class="pBtnMain" onclick="goPage('Groupware')">Groupware</p>
</div>
<div id="divTitle">WELCOME</div>
<div id="divImage">
<img src="mainbackground.jpg" class="imgSource"/>
</div>

<div id="divFooter">
	<span class="spanLogo"></span>
	<span class="spanFooter"><i class="fa fa-copyright" aria-hidden="true"></i> KPU 2016531047 JMJ</span>
	<span class="spanFooter"><i class="fa fa-font" aria-hidden="true"></i> Nanum Gothic (GoogleAPI : earlyaccess)</span>
	<span class="spanFooter"><i class="fa fa-file-image-o" aria-hidden="true"></i> Font Awesome</span>
	<span class="spanFooter"><i class="fa fa-phone" aria-hidden="true"></i> 010-2113-9911</span>
	<span class="spanFooter"><i class="fa fa-envelope" aria-hidden="true"></i> alswjd135792@gmail.com</span>
</div>
</body>
</html>