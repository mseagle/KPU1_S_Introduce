<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>2016531047</title>
<script src="https://use.fontawesome.com/e7683fce0b.js"></script>
<script src="Master.js"></script>
<link rel="stylesheet" type="text/css" href="Master.css">
<link rel="stylesheet" type="text/css" href="Task.css">	
</head>
<body>
<div id="divHeader">
	<span class="spanLogoMain" onclick="goHome()"></span>
	<span id="spanHome" onclick="goHome()">Summer Term Project</span>
	<p class="pBtnMainEnd" onclick="goPage('Project')">Project</p>
	<p class="pBtnMainLine2" onclick="goPage('ProductManagement')">Product Management</p>
	<p class="pBtnMain" onclick="goPage('Task')">Task</p>
	<p class="pBtnMain" onclick="goPage('Groupware')">Groupware</p>
</div>
<div id="divTitle">Task</div>
<div id="divContent">
<div id="divLine">
<div class="divLine"></div>
<div onclick="SetDescription('2')"><div class="divNumber">2</div><div class="divDot"></div></div>
<div onclick="SetDescription('3')"><div class="divNumber">3</div><div class="divDot"></div></div>
<div onclick="SetDescription('4')"><div class="divNumber">4</div><div class="divDot"></div></div>
<div onclick="SetDescription('5')"><div class="divNumber">5</div><div class="divDot"></div></div>
<div onclick="SetDescription('6')"><div class="divNumber">6</div><div class="divDot"></div></div>
<div onclick="SetDescription('7')"><div class="divNumber">7</div><div class="divDot"></div></div>
<div onclick="SetDescription('8')"><div class="divNumber">8</div><div class="divDot"></div></div>
<div onclick="SetDescription('9')"><div class="divNumber">9</div><div class="divDot"></div></div>
<div onclick="SetDescription('10')"><div class="divNumber">10</div><div class="divDot"></div></div>
<div onclick="SetDescription('11')"><div class="divNumber">11</div><div class="divDot"></div></div>
<div onclick="SetDescription('12')"><div class="divNumber">12</div><div class="divDot"></div></div>
<div class="divProgress"><i class="fa fa-angle-right fa-4x" aria-hidden="true"></i></div>
</div>
<div class="divDescription">날짜나 막대바를 클릭하면 각 버튼 옆에 내용이 나타납니다. 버튼 클릭 시 해당 메뉴의 페이지로 이동하게 됩니다.</div>
<div id="divRedirect">
<p onclick="javascript:location.href='ProductManagement.jsp'">Product Management</p>
<div id="divDescriptionProd"></div>
<p onclick="javascript:location.href='Project.jsp'">Project</p>
<div id="divDescriptionProj"></div>
</div>
</div>
<div id="divFooter">
	<span class="spanLogo"></span>
	<span class="spanFooter"><i class="fa fa-copyright" aria-hidden="true"></i> KPU 2016531047 JMJ</span>
	<span class="spanFooter"><i class="fa fa-font" aria-hidden="true"></i> Nanum Gothic (GoogleAPI : earlyaccess)</span>
	<span class="spanFooter"><i class="fa fa-file-image-o" aria-hidden="true"></i> Font Awesome</span>
	<span class="spanFooter"><i class="fa fa-phone" aria-hidden="true"></i> 010-2113-9911</span>
	<span class="spanFooter"><i class="fa fa-envelope" aria-hidden="true"></i> alswjd135792@gmail.com</span>
</div>
<script>
function SetDescription(pMonth) {
	var strProd = "";
	var strProj = "";
	switch (pMonth) {
	case "2":
		strProd = "제품 백업 시작";
		strProj = "";
		break;
	case "3":
		strProd = "배포문서 관리 및 제품 배포 시작";
		strProj = "";
		break;
	case "4":
		strProd = "제품관리, 라이선스 적용 시작";
		strProj = "";
		break;
	case "5":
		strProd = "제품관리";
		strProj = "현대중공업 기본 설치와 에디터 기능 추가 작업";
		break;
	case "6":
		strProd = "신입사원교육시작 ";
		strProj = "비엘인터내셔날 그룹웨어 작업, 일동후디스 구글일정 적용";
		break;
	case "7":
		strProd = "";
		strProj = "이스트스프링 그룹웨어 작업,";
		break;
	case "8":
		strProd = "";
		strProj = "미래에셋 대우 커스터마이징 작업 ";
		break;
	case "9":
		strProd = "신입사원 교육 ";
		strProj = "골프존유원홀딩스 계약관리 SI 작업 ";
		break;
	case "10":
		strProd = "";
		strProj = "";
		break;
	case "11":
		strProd = "";
		strProj = "";
		break;
	case "12":
		strProd = "";
		strProj = "";
		break;
	}
	document.getElementById("divDescriptionProd").textContent = strProd;
	document.getElementById("divDescriptionProj").textContent = strProj;
}

</script>
</body>
</html>