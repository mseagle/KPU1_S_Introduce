<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>2016531047</title>
<script src="https://use.fontawesome.com/e7683fce0b.js"></script>
<script src="Master.js"></script>
<link rel="stylesheet" type="text/css" href="Master.css">
<link rel="stylesheet" type="text/css" href="Groupware.css">	
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
<div id="divHeader">
	<span class="spanLogoMain" onclick="goHome()"></span>
	<span id="spanHome" onclick="goHome()">Summer Term Project</span>
	<p class="pBtnMainEnd" onclick="goPage('Project')">Project</p>
	<p class="pBtnMainLine2" onclick="goPage('ProductManagement')">Product Management</p>
	<p class="pBtnMain" onclick="goPage('Task')">Task</p>
	<p class="pBtnMain" onclick="goPage('Groupware')">Groupware</p>
</div>
<div id="divTitle">Groupware</div>
<div class="divContentMain">
<div class="divThmbs">
<ul class="ulThumb">
    <li class="liThumb"><span class="spanThumb"><a href="#slideBoard" onclick="return ChangeImage('Board');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span></li>
    <li class="liThumb"><span class="spanThumb"><a href="#slideSchedule" onclick="return ChangeImage('Schedule');"><i class="fa fa-calendar" aria-hidden="true"></i></a></span></li>
    <li class="liThumb"><span class="spanThumb"><a href="#slideResource" onclick="return ChangeImage('Resource');"><i class="fa fa-car" aria-hidden="true"></i></a></span></li>
    <li class="liThumb"><span class="spanThumb"><a href="#slideDocument" onclick="return ChangeImage('Document');"><i class="fa fa-file-text" aria-hidden="true"></i></a></span></li>
    <li class="liThumb"><span class="spanThumb"><a href="#slideCommunity" onclick="return ChangeImage('Community');"><i class="fa fa-comments-o" aria-hidden="true"></i></a></span></li>
    <li class="liThumb"><span class="spanThumb"><a href="#slideSurvey" onclick="return ChangeImage('Survey');"><i class="fa fa-pie-chart" aria-hidden="true"></i></a></span></li>
</ul>
</div>
<div class="divText" id="divText">
좌측의 아이콘을 클릭하면 내용이 나타납니다.
</div>
</div>
<div id="divFooter">
	<span class="spanLogo"></span>
	<span class="spanFooter"><i class="fa fa-copyright" aria-hidden="true"></i> KPU 2016531047 JMJ</span>
	<span class="spanFooter"><i class="fa fa-font" aria-hidden="true"></i> Nanum Gothic (GoogleAPI : earlyaccess)</span>
	<span class="spanFooter"><i class="fa fa-file-image-o" aria-hidden="true"></i> Font Awesome</span>
	<span class="spanFooter"><i class="fa fa-phone" aria-hidden="true"></i> 010-2113-9911</span>
	<span class="spanFooter"><i class="fa fa-envelope" aria-hidden="true"></i> alswjd135792@gmail.com</span>
</div>
</body>
<script type="text/javascript">
function ChangeImage(pType){
	var slideText = "";
	switch (pType) {
		case "Board":
			slideText = "회사의 알림사항 및 일반적인 게시사항과 카페에서 구성원간 공유되어야 할 게시물을 등록, 조회 관리하여 해당 구성원들의 원활한 커뮤니케이션의 토대를 제공합니다. 업무성격에 따라 일반 게시판, 공지 게시판, 자료 게시판, 앨범 게시판, 동영상 게시판, 익명 게시판 등으로 게시글의 성격에 따라 게시판 종류를 구분하고 종류에 따른 옵션들을 제공합니다.게시판 별로 다양한 옵션관 상세한 권한을 설정할 수 있고 게시글에도 권한을 설정하여 효율적으로 자료를 관리할 수 있도록 지원합니다.";
			break;
		case "Schedule":
			slideText = "개인이 작성한 일정정보들을 관리하는 개인일정과 그룹차원의 일정, 회사일정, 자신이 속한 부서의일정, 임원의 일정 등을 조회하거나 관리할 수 있으며, 자신이 속한 커뮤니티 혹은 자신이 구성원으로 포함된 프로젝트에서의 일정정보 등을 조회할 수 있습니다.";
			break;
		case "Resource":
			slideText = "회사 혹은 그룹사의 모든 유용한 공용자원(회의실, 차량, 카메라 등)을 효율적으로 관리하고, 사용자들이 번거로운 절차 없이 직관적으로 자원을 예약하고 간편하게 반납하며 현재 자원의 상태를 실시간으로 볼 수 있어 자원 사용을 위한 불필요한 시간을 줄임으로써 업무 능률을 향상시킵니다.";
			break;
		case "Document":
			slideText = "회사에서 사용하는 문서들을 미리 지정된 분류, 버전별로 관리할 수 있다. 전자결재 시스템에서 완료된 문서들은 문서관리로 이관되어 관리할 수 있으며, 문서별 권한설정, 폐기날짜 지정으로 문서의 보안을 철저히 할 수 있습니다.";
			break;
		case "Community":
			slideText = "회사 구성원들의 취미나 관심 있는 공통 주제를 함께 공유하고 관련된 게시판이나 기타 여러 가지 기능으로 해당 커뮤니티 회원들의 원활한 커뮤니케이션이 이루어질 수 있는 토대를 제공합니다.";
			break;
		case "Survey":
			slideText = "전자설문은 기존 설문조사를 온라인으로 진행함으로써 보다 편리하게 직원들의 의견을 수렴하고 설문의 결과로 얻어진 통계치 들을 활용하여 보다 효율적인 업무 추진 성과를 얻을 수 있습니다. ";
			break;
	}
	document.getElementById("divText").textContent = slideText;
    return true;
}

</script>
</html>